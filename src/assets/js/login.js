(function($) {
    var $loginForm = $('form#loginform');
    var $loginSubmit = $loginForm.find('#login-submit');
    var $loginSpinner = $loginForm.find('.ajax-notifications .dgk-spinner');
    var $loginError = $loginForm.find('.ajax-notifications .error');
    var $loginSuccess = $loginForm.find('.ajax-notifications .success');

    $loginForm.submit(function(e) {
        e.preventDefault();
        login();
    });

    function login() {
        $.ajax({
            type: 'post',
            url: footerGlobalObject.ajax_url,
            dataType: 'json',
            data: {
                'action': 'dgk_ajax_login',
                'nonce': $loginForm.find('#dgk-ajax-login-nonce').val(),
                'user_login': $loginForm.find('#user_login').val(),
                'user_password': $loginForm.find('#user_password').val(),
                'g-recaptcha-response': $loginForm.find('.g-recaptcha-response').val(),
                'rememberme': $loginForm.find('#rememberme').is(':checked'),
            },
            success: function(response) {
                if (!response.loggedin) {
                    $loginError.text(response.message).show();
                    $loginSubmit.show();
                } else {
                    $loginSuccess.html(response.message).show();
                    window.location.replace(response.redirect);
                }
            },
            error: function(response) {
                console.log(response);
            },
            beforeSend: function(qXHR, settings) {
                $loginError.hide();
                $loginSuccess.hide();
                $loginSubmit.hide();
                $loginSpinner.show();
            },
            complete: function() {
                $loginSpinner.hide();
                if(grecaptcha.lenght){

                    $('.anr_captcha_field').each(function(index) {
                        grecaptcha.reset(index);
                    })
                }
            }
        });
    }
})(jQuery);