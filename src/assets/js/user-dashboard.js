(function($) {
    var $projectSelect = $('select#project-select');

    if (!$projectSelect.length) {
        return false;
    } else {
        $projectSelect.change(function(e) {
            var project_id = $(this).val();
            setParameters('project_id', project_id, true);
        });
    }

    var $mvpsRow = $('.row.project-mvps');
    var animate = $mvpsRow.length ? true : false;
    var scrollTimer;
    $(window).scroll(function() {
        if (!animate) return false;

        if (scrollTimer) window.clearTimeout(scrollTimer);

        scrollTimer = window.setTimeout(function() {
            var element = document.querySelector('.row.project-mvps');
            var position = element.getBoundingClientRect();

            // checking for partial visibility
            if (position.top < window.innerHeight && position.bottom >= 0) {
                $('.progress').each(function() {
                    var $bar = $(this).find(".progress-bar");
                    var $val = $(this).find("span");
                    var perc = parseInt($val.text(), 10);

                    $bar.animate({ width: perc + '%' }, {
                        duration: 2500,
                        easing: "swing",
                        step: function(p) {
                            $val.text(p | 0);
                        }
                    });
                });

                $('.progress-circle').each(function() {
                    var $bar = $(this).find(".bar");
                    var $val = $(this).find("span");
                    var perc = parseInt($val.text(), 10);

                    $bar.show();
                    $bar.animate({ p: perc }, {
                        duration: 3000,
                        easing: "swing",
                        step: function(p) {
                            $bar.css({
                                transform: "translateZ(1px) rotate(" + (45 + (p * 1.8)) + "deg)", // 100%=180° so: ° = % * 1.8
                                // 45 is to add the needed rotation to have the green borders at the bottom
                            });
                            $val.text(p | 0);
                        }
                    });
                });
                animate = false;
            }
        }, 100);
    });

    $(document).on('page-loaded', function() {
        $(window).trigger('scroll');
    });

})(jQuery);