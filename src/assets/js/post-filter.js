(function($) {
    var $dataContainer = $('.data-item-container');

    if ($dataContainer.length) {
        var $spinner = $('.ajax-notifications .dgk-spinner');
        var $endReached = $('.ajax-notifications .end-reached');
        var $loadMore = $('button.load-more');
        var $filterControls = $('.filter-controls button');
        var $filterLinks = $('.filter-link');

        loadData();

        $loadMore.click(function(e) {
            e.preventDefault();
            loadData();
        });

        $filterLinks.click(function(e) {
            e.preventDefault();
            var $this = $(this);
            $filterLinks.parent().removeClass('active');
            $this.parent().addClass('active');
            $dataContainer.empty();
            $loadMore.hide();
            $endReached.hide();
            $dataContainer.attr('data-taxonomy', $this.attr('data-taxonomy'));
            $dataContainer.attr('data-termslug', $this.attr('data-termslug'));
            $dataContainer.attr('data-maxpages', $this.attr('data-maxpages'));
            $dataContainer.attr('data-paged', 1);
            $filterControls.click();
            setParameters('page', 1);
            loadData();
        });

        function loadData() {
            var paged = $dataContainer.attr('data-paged');
            var postType = $dataContainer.attr('data-posttype');
            var contentTemplate = $dataContainer.attr('data-content-template');
            var taxonomy = $dataContainer.attr('data-taxonomy');
            var termslug = $dataContainer.attr('data-termslug');
            var perPage = $dataContainer.attr('data-perpage');
            var maxPages = $dataContainer.attr('data-maxpages');
            var lastIndex = $('.post-index').length ? $('.post-index').last().val() : 0;
            var search = $dataContainer.attr('data-search');
            //post query
            var data = {
                paged: paged,
                postType: postType,
                contentTemplate: contentTemplate,
                taxonomy: taxonomy,
                termslug: termslug,
                perPage: perPage,
                lastIndex: lastIndex,
                search: search
            }

            if (paged > maxPages) {
                $endReached.show();
            } else {
                $.ajax({
                    type: 'post',
                    url: footerGlobalObject.ajax_url,
                    data: {
                        action: 'dgk_load_more',
                        data: data
                    },
                    success: function(response) {
                        var data = $(response);
                        if (data.length) {
                            if (paged > 1) {
                                $dataContainer.append('<div class="page-count text-center">-Página ' + paged + '-</div>');
                                setParameters('page', paged);
                            }
                            $dataContainer.append(data);

                        }
                    },
                    error: function(response) {
                        console.log(response);
                    },
                    beforeSend: function(qXHR, settings) {
                        $spinner.show();
                        $loadMore.hide();
                    },
                    complete: function() {
                        $spinner.hide();
                        paged++;
                        $dataContainer.attr('data-paged', paged);
                        if (paged > maxPages) {
                            $endReached.show();
                        } else {
                            $loadMore.show();
                        }

                        $dataContainer.find('[data-hover]').each(function(index, link){
                            link.addEventListener('mouseenter', () => cursor.enter() );
                            link.addEventListener('mouseleave', () => cursor.leave() );
                            link.addEventListener('click', () => cursor.click() );
                        });
                    }
                });
            }
        }
    }
})(jQuery);