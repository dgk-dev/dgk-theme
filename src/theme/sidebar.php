<!-- side-column -->
<div class="side-column">
	<?php if ( is_active_sidebar( 'blog-sidebar' ) ) : ?>
		<?php dynamic_sidebar( 'blog-sidebar' ); ?>
	<?php endif; ?>
</div>
<!-- /side-column -->
