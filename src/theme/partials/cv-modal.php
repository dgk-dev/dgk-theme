<!-- Modal -->
<div class="modal fade" id="CVModal" tabindex="-1" role="dialog" aria-labelledby="CVModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close" data-hover><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="CVModalLabel"><strong>ENVÍANOS TU CV</strong></h4>
      </div>
      <div class="modal-body">
        <?php echo do_shortcode('[contact-form-7 title="Bolsa de Trabajo"]'); ?>
      </div>
    </div>
  </div>
</div>