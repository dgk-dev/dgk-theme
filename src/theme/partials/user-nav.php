<!-- Fixed navbar -->
<?php if (is_user_logged_in()) {global $current_user;wp_get_current_user();} ?>
<nav class="collapse navbar-collapse bs-navbar-collapse user-nav">
  <ul class="nav navbar-nav navbar-left">
    <li class="nav-item">
      <a class="nav-link <?php echo is_page('estatus') ? 'active' : '' ?>" href="<?php echo get_permalink(get_page_by_path('user-dashboard/estatus')); ?>" rel="keep-params" data-hover>ESTATUS</a>
    </li>
    <li class="nav-item">
      <a class="nav-link <?php echo is_page('alcance') ? 'active' : '' ?>" href="<?php echo get_permalink(get_page_by_path('user-dashboard/alcance')); ?>" rel="keep-params" data-hover>ALCANCE</a>
    </li>
    <li class="nav-item">
      <a class="nav-link <?php echo is_page('facturas') ? 'active' : '' ?>" href="<?php echo get_permalink(get_page_by_path('user-dashboard/facturas')); ?>" rel="keep-params" data-hover>FACTURAS</a>
    </li>
  </ul>
  <ul class="nav navbar-nav navbar-right">
    <li class="dropdown">
      <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">MI CUENTA <span class="caret"></span></a>
      <ul class="dropdown-menu dropdown-menu-right">
        <li class="username"><span>Hola, <?php echo $current_user->display_name; ?></span></li>
        <li role="separator" class="divider"></li>
        <li><a href="<?php echo wp_logout_url(home_url()); ?>">Cerrar sesión</a></li>
      </ul>
    </li>
  </ul>
</nav>