<!-- Modal -->
<div class="modal fade" id="LoginModal" tabindex="-1" role="dialog" aria-labelledby="LoginModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close" data-hover><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="LoginModalLabel"><strong>ACCEDE A TU CUENTA EN dgk</strong></h4>
            </div>
            <div class="modal-body">
                <form id="loginform">
                    <?php wp_nonce_field('dgk-ajax-login-nonce', 'dgk-ajax-login-nonce'); ?>
                    <div class="form-group">
                        <label for="user_login">Correo electrónico</label>
                        <input type="text" class="form-control" id="user_login" name="user_login">
                    </div>
                    <div class="form-group">
                        <label for="user_password">Contraseña</label>
                        <input type="password" class="form-control" id="user_password" name="user_password">
                    </div>
                    <div class="checkbox">
                        <label>
                            <input id="rememberme" name="rememberme" type="checkbox"> Recordarme
                        </label>
                    </div>
                    <?php function_exists('anr_verify_captcha') ? do_action( 'anr_captcha_form_field' ) : null; ?>
                    <div class="ajax-notifications">
                        <div class="dgk-spinner">
                            <div class="rect rect1"></div>
                            <div class="rect rect2"></div>
                            <div class="rect rect3"></div>
                            <div class="rect rect4"></div>
                            <div class="rect rect5"></div>
                        </div>
                        <div class="error text-center"></div>
                        <div class="success text-center"></div>
                    </div>
                    <div class="text-center">
                        <button id="login-submit" type="submit" class="btn btn-primary">INGRESAR</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>