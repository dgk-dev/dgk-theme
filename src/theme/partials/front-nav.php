<!-- Fixed navbar -->
<nav class="collapse navbar-collapse bs-navbar-collapse">
  <div class="page-cont navbar-right">
    01
  </div>
  <ul class="nav navbar-nav navbar-right">
    <li class="nav-item">
      <a class="nav-link <?php echo is_front_page() ? 'active' : '' ?>" href="<?php echo is_front_page() ? '' : home_url(); ?>#porquedgk" data-hover>¿POR QUÉ <span class="text-lowercase">DGK</span>?</a>
    </li>
    <li class="nav-item">
      <a class="nav-link" href="<?php echo is_front_page() ? '' : home_url(); ?>#quehacemos" data-hover>¿QUÉ HACEMOS?</a>
    </li>
    <li class="nav-item">
      <a class="nav-link" href="<?php echo is_front_page() ? '' : home_url(); ?>#filosofia" data-hover>FILOSOFÍA</a>
    </li>
    <li class="nav-item">
      <a class="nav-link" href="<?php echo is_front_page() ? '' : home_url(); ?>#clientes" data-hover>CLIENTES</a>
    </li>
    <li class="nav-item">
      <a class="nav-link nav-link__contacto" href="<?php echo is_front_page() ? '' : home_url(); ?>#contacto" data-hover>
        <span class="button__bg"></span>
        <span>CONTACTO</span>
      </a>
    </li>
  </ul>
  <div class="top-bar">
    <ul class="right">
      <li class="<?php echo is_home()  ? 'active' : ''; ?>"><a href="<?php echo get_permalink(get_option('page_for_posts')); ?>" data-hover>Blog</a></li>
      <li class="<?php echo is_post_type_archive('dgk-job')  ? 'active' : ''; ?>"><a href="<?php echo get_post_type_archive_link('dgk-job'); ?>" data-hover>Bolsa de trabajo</a></li>
      <?php if (is_user_logged_in()) : global $current_user;
        wp_get_current_user(); ?>
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Mi cuenta <span class="caret"></span></a>
          <ul class="dropdown-menu dropdown-menu-right">
            <li class="username"><span>Hola, <?php echo $current_user->display_name; ?></span></li>
            <li role="separator" class="divider"></li>
            <?php if (current_user_can('manage_options')) : ?>
              <li><a href="<?php echo get_admin_url(); ?>">Panel de administración</a></li>
            <?php else : ?>
              <li><a href="<?php echo get_permalink(get_page_by_path('user-dashboard')); ?>">Dashboard</a></li>
            <?php endif; ?>
            <li><a href="<?php echo wp_logout_url(home_url()); ?>">Cerrar sesión</a></li>
          </ul>
        </li>
      <?php else : ?>
        <li><a href="#" data-toggle="modal" data-target="#LoginModal" data-hover>Login</a></li>
      <?php endif; ?>
    </ul>
  </div>
</nav>