<div class="row">
    <div class="col-xs-12 text-center no-data">
        <h3>No puedes acceder a esta página</h3>
        <p>Puedes intentar <a href="#" data-toggle="modal" data-target="#LoginModal" data-hover>ingresar</a> e intentarlo nuevamente</p>
    </div>
</div>