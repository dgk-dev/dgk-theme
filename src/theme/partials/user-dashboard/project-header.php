<?php
global $current_user;
wp_get_current_user();
$args = array(
    'post_type'       => 'dgk-project',
    'post_status'     => 'publish',
    'posts_per_page'  => -1,
    'order'           => 'ASC',
    'meta_query'      => array(
        array(
            'key'         => 'dgk-project-owner',
            'value'       => $current_user->ID,
        ),
    )
);

$userProjects = get_posts($args);
$currentProject = get_query_var('current_project');
$currentPage = get_query_var('current_page');
?>
<div class="row project-select">
    <div class="col-xs-12">
        <h2>Hola, <?php echo $current_user->display_name; ?></h2>
        <?php if (!empty($userProjects)) : ?>
            <select class="form-control" name="project-select" id="project-select" data-hover>
                <?php foreach ($userProjects as $user_project) : ?>
                    <option value="<?php echo $user_project->ID; ?>" <?php echo $currentProject && $currentProject == $user_project->ID ? 'selected="selected"' : '' ?>>Proyecto: <?php echo $user_project->post_title; ?></option>
                <?php endforeach; ?>
            </select>
        <?php endif; ?>
        <div class="project-last-update text-right">
            <span class="glyphicon glyphicon-calendar" aria-hidden="true"></span> Última actualizacion: <?php echo get_the_modified_time('j F, Y. h:i a'); ?>
        </div>
    </div>
</div>
<div class="row project-title">
    <div class="col-xs-12">
        <h1 class="page-title text-center">
            <?php echo $currentPage; ?>
            <svg class="formas" viewBox="0 0 305 228" version="1.1">
                <g id="Page-1" fill="#f2d01e" fill-rule="evenodd">
                    <polygon class="animated fadeInRight" id="Fill-1" points="230.971641 0 208 30.4993215 230.972982 61 249 61 226.022995 30.4993215 248.998659 0"></polygon>
                    <polygon class="animated fadeInRight delay-2s" id="Fill-2" points="163 41 141.014378 41 113 77.5020358 141.014378 114 163 114 134.978788 77.5020358"></polygon>
                    <polygon class="animated fadeInRight" id="Fill-3" points="231 83 195.383246 83 150 143.001356 195.383246 203 231 203 185.604647 143.001356"></polygon>
                    <polygon class="animated fadeInRight delay-3s" id="Fill-4" points="95 60 75.2127564 60 50 93.5 75.2127564 127 95 127 69.7805155 93.5"></polygon>
                    <polygon class="animated fadeInRight" id="Fill-5" points="104.214694 161 79 194.5 104.214694 228 124 228 98.7812696 194.5 124 161"></polygon>
                    <polygon class="animated fadeInRight delay-4s" id="Fill-6" points="10.0855778 122 0 136 0 136.002758 10.0855778 150 18 150 7.91181581 136.001379 18 122"></polygon>
                    <polygon class="animated fadeInRight" id="Fill-7" points="204 47 196.086151 47 186 61 196.086151 75 204 75 193.912546 61"></polygon>
                    <polygon class="animated fadeInRight delay-5s" id="Fill-8" points="181 195 173.085578 195 163 209 173.085578 223 181 223 170.913119 209"></polygon>
                    <polygon class="animated fadeInRight" id="Fill-9" points="86 136 71.9302356 136 54 159 71.9302356 182 86 182 68.0656149 159"></polygon>
                    <polygon class="animated fadeInRight" id="Fill-10" points="267 170 255.129181 170 240 189.5 255.129181 209 267 209 251.869453 189.5"></polygon>
                    <polygon class="animated fadeInRight delay-2s" id="Fill-11" points="127 108 115.127815 108 100 127.499329 115.127815 147 127 147 111.868087 127.499329"></polygon>
                    <polygon class="animated fadeInRight" id="Fill-12" points="290.929461 115 273 138.001352 290.929461 161 305 161 287.06639 138.001352 305 115"></polygon>
                </g>
            </svg>
        </h1>
    </div>
</div>