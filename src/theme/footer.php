      <footer>
        <div class="row">
          <div class="col-sm">
            <img src="<?php echo get_template_directory_uri(); ?>/img/lg-dgk-bttm.svg" alt="logo dgk">
          </div>
          <div class="col-sm text-center">
            <a href="tel:55 7877 4186" data-hover>55 7877 4186</a>
          </div>
          <div class="col-sm text-center">
            <a href="mailto:clientes@dgk.com.mx" target="_BLANK" data-hover>clientes@dgk.com.mx</a>
          </div>
          <div class="col-sm text-right social-media">
            <a href="https://www.linkedin.com/company/dgk-mexico" target="_BLANK" data-hover>
              <img src="<?php echo get_template_directory_uri() ?>/img/linkedin_Icon.svg" alt="linkedin icon">
            </a>
            <a href="https://www.facebook.com/dgkmx/" target="_BLANK" data-hover>
              <img src="<?php echo get_template_directory_uri() ?>/img/facebook_Icon.svg">
            </a>
            <!-- <a href="#" data-hover>
              <img src="<?php echo get_template_directory_uri() ?>/img/twitter_Icon.svg">
            </a> -->
            <a href="https://www.instagram.com/dgkmx/" target="_BLANK" data-hover>
              <img src="<?php echo get_template_directory_uri() ?>/img/instagram_Icon.svg">
            </a>
            <!-- <a href="#" data-hover>
              <img src="<?php echo get_template_directory_uri() ?>/img/google_logo.svg">
            </a> -->
          </div>
        </div>
        <hr>
        <div class="row">
          <div class="col-sm">
            © <?php echo date('Y'); ?> dgk. Todos los derechos reservados.
          </div>
          <div class="col-sm text-right">
            <a href="<?php echo get_permalink(get_page_by_path('aviso-de-privacidad')); ?>" data-hover>Aviso de privacidad</a>
          </div>
        </div>
      </footer>
      
      <?php if(!is_user_logged_in()) get_template_part( 'partials/login-modal' ); ?>

      </div>
      <div class="cursor">
        <div class="cursor__inner cursor__inner--circle" style="transform: translateX(828px) translateY(77px) scale(1); opacity: 1;"></div>
        <div class="cursor__inner cursor__inner--dot" style="transform: translateX(835px) translateY(84px);"></div>
      </div>

      <!-- build: jquery, bootstrap, imagesloaded, wow (with init), modernizr, action in bundle -->
      <?php wp_footer() ?>
      </body>

      </html>