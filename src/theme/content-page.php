<article class="page">
	<h1 class="page-title"><?php the_title(); ?></h1>
	<hr>
	<?php the_content(); ?>
</article>
