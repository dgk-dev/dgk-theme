	
<?php /* Template Name: Parent page -> First child */

$pagekids = get_pages('child_of=' . $post->ID . '&sort_column=ID');
if ($pagekids) {
    $firstchild = $pagekids[0];
    wp_redirect(get_permalink($firstchild->ID));
} else {
    // Do whatever templating you want as a fall-back.
}
