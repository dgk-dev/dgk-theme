<?php get_header(); ?>
<!-- container -->
<main class="container" role="main">
    <!-- site-content -->
    <section class="site-content">
        <article class="page">
            <?php if (is_user_logged_in()) : ?>
                <?php $project = get_dgk_project(); ?>
                <?php if ($project->have_posts()) : ?>
                    <?php while ($project->have_posts()) : $project->the_post(); ?>
                        <?php
                        set_query_var('current_project', get_the_ID());
                        set_query_var('current_page', 'Alcance');
                        get_template_part('partials/user-dashboard/project-header');
                        ?>
                        <div class="row project-scope">
                            <div class="col-xs-12">
                                <?php
                                $scope = get_post_meta($post->ID, 'dgk-project-scope', true);
                                if ($scope) : ?>
                                    <div class="entry-content">
                                        <?php echo do_shortcode(wpautop($scope, true)); ?>
                                    </div>
                                <?php else : ?>
                                    <section class="no-results text-center">
                                        <h3 class="page-title"><?php _e('Aún no hay alcance definido para este proyecto', 'dgk-theme'); ?></h3>
                                    </section>
                                <?php endif; ?>
                            </div>
                        </div>
                    <?php endwhile;
                    $project->reset_postdata(); ?>
                <?php else : ?>
                    <?php get_template_part('content', 'none'); ?>
                <?php endif; ?>
            <?php else : ?>
                <?php get_template_part('partials/user-dashboard/content', 'no-loggedin'); ?>
            <?php endif; ?>
        </article>
    </section>
    <!-- /site-content -->
</main>
<!-- /container -->
<?php get_footer(); ?>