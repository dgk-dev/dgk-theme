jQuery(function($) {
    // Funciones owner
    $('#dgk-project-owner').select2({
        allowClear: true,
        placeholder: "Selecciona un usuario",
        ajax: {
            url: ajaxurl, // AJAX URL is predefined in WordPress admin
            dataType: 'json',
            delay: 250, // delay in ms while typing when to perform a AJAX search
            data: function(params) {
                return {
                    q: params.term, // search query
                    action: 'dgk_project_get_owner' // AJAX action for admin-ajax.php
                };
            },
            processResults: function(data) {
                var options = [];
                if (data) {
                    // data is the array of arrays, and each of them contains ID and the Label of the option
                    $.each(data, function(index, text) { // do not forget that "index" is just auto incremented value
                        options.push({ id: text.id, text: text.name });
                    });
                }
                return {
                    results: options
                };
            },
            cache: true
        },
        minimumInputLength: 3 // the minimum of symbols to input before perform a search
    });


    //Funciones MVPs
    var $mvpIndex = $('#mvp-index');
    var $mvpAppend = $('#mvp-append');
    var $addMVP = $('#add-mvp');
    $addMVP.click(function(e) {
        e.preventDefault();
        var mvpId = $mvpIndex.val();
        mvpId++;
        $.ajax({
            url: ajaxurl,
            data: {
                action: 'dgk_project_add_mvp',
                id: mvpId
            },
            success: function(response) {
                $mvpAppend.append(response);
                refreshMVP();
                $mvpAppend.accordion('refresh');
                $mvpIndex.val(mvpId);
            },
            error: function(response) {
                console.log('error', response);
            },
            beforeSend: function(qXHR, settings) {

            },
            complete: function() {

            }
        });
    });

    $mvpAppend.accordion({
        header: "> div > h3",
        collapsible: true,
        heightStyle: "content",
        active: false,
        clearStyle: true,
        autoHeight: false
    }).sortable({
        axis: "y",
        handle: "h3",
        placeholder: 'dgk-project-mvp-placeholder',
        stop: function(event, ui) {
            refreshMVP();
            // Refresh accordion to handle new order
            ui.item.children("h3").triggerHandler("focusout");
            $mvpAppend.accordion('refresh');
        }
    });

    $('body').on('click', 'a.remove-mvp', function(e) {
        e.preventDefault();
        var target = $(this).attr('data-mvp-id');
        $(target).remove();
        refreshMVP();
    });

    $('body').on('keyup', '.mvp-title-input', function(e) {
        var $this = $(this);
        var target = $this.attr('data-mvp-id');
        var text = $this.val();
        $(target).find('.mvp-title').text(text);
    });

    function initTinyMCE(id, reinit = false) {
        var target = 'dgk-project-mvp-editor-' + id;
        tinyMCEGlobalSettings = tinyMCEPreInit.mceInit.content;
        tinyMCEGlobalSettings.resize = true;
        tinyMCEGlobalSettings.theme_advanced_resizing = true;
        tinyMCE.init(tinyMCEGlobalSettings);
        if (reinit) tinyMCE.execCommand('mceRemoveEditor', false, target);
        tinyMCE.execCommand('mceAddEditor', false, target);
        quicktags({ id: target });
    }

    function refreshMVP() {
        $('.dgk-project-mvp-container').each(function(index) {
            var $this = $(this);
            var thisid = $this.attr('data-index');
            $this.find('.mvp-id').text(index + 1);
            initTinyMCE(thisid, true);
        });
    }

    // Funciones facturas
    var $invoiceAppend = $('#invoice-append');
    var $invoices = $('#dgk-project-invoices-hidden');
    var $addInvoice = $('#add-invoice');

    $addInvoice.click(function(e) {
        e.preventDefault();

        // Sets up the media library frame
        metaImageFrame = wp.media.frames.metaImageFrame = wp.media();

        // Runs when an image is selected.
        metaImageFrame.on('select', function() {
            // Grabs the attachment selection and creates a JSON representation of the model.
            var media_attachment = metaImageFrame.state().get('selection').first().toJSON();

            var allMedia = $invoices.val() ? $invoices.val().split(',') : [];

            allMedia.push(media_attachment.id);
            // Sends the attachment URL to our custom image input field.
            $invoices.val(allMedia.join(','));

            var html = '<div class="dgk-project-invoice-container" id="dgk-project-invoice-' + media_attachment.id + '" data-index="' + media_attachment.id + '">' +
                '<a class="invoice-link" href="' + media_attachment.url + '" target="_BLANK">' + media_attachment.title + '</a><br>' +
                '<a class="remove-invoice" href="#" data-invoice-id="#dgk-project-invoice-' + media_attachment.id + '">Eliminar factura</a>' +
                '</div>';

            $invoiceAppend.append(html);
        });

        // Opens the media library frame.
        metaImageFrame.open();
    });

    $invoiceAppend.sortable({
        axis: "y",
        placeholder: 'dgk-project-invoice-placeholder',
        stop: function(event, ui) {
            refreshInvoices();
        }
    });

    $('body').on('click', 'a.remove-invoice', function(e) {
        e.preventDefault();
        var target = $(this).attr('data-invoice-id');
        $(target).remove();
        refreshInvoices();
    });

    function refreshInvoices() {
        var newInvoices = [];
        $('.dgk-project-invoice-container').each(function(index) {
            var $this = $(this);
            var thisid = $this.attr('data-index');
            newInvoices.push(thisid);
        });
        $invoices.val(newInvoices.join(','));
    }
});