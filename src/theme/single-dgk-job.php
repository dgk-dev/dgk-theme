<?php get_header(); ?>
<!-- container -->
<div class="container">
	<!-- site-content -->
	<section class="site-content single">
		<?php
		if (have_posts()) : while (have_posts()) : the_post(); ?>
			<article class="job-single">
				<h1 class="single-title"><?php the_title(); ?></h1>
				<hr>
				<div class="job-single-data">
					<?php
					$area = get_the_terms(get_the_ID(), 'area') ? get_the_terms(get_the_ID(), 'area')[0]->name : '';
					$workstation = get_the_terms(get_the_ID(), 'workstation') ? get_the_terms(get_the_ID(), 'workstation')[0]->name : '';
					$location = get_post_meta(get_the_ID(), 'dgk-job-location', true);
					$scholarship = get_post_meta(get_the_ID(), 'dgk-job-scholarship', true);
					?>
					<ul>
						<?php if($area || $workstation): ?>
							<li>
								<span class="glyphicon glyphicon-bookmark" aria-hidden="true"></span> <?php echo $area ? $area : ''; echo $workstation ? ' - '.$workstation : ''; ?>
							</li>
						<?php endif; ?>
						<?php if($location): ?>
							<li>
								<span class="glyphicon glyphicon-home" aria-hidden="true"></span> <?php echo $location ?>
							</li>
						<?php endif; ?>
						<?php if($scholarship): ?>
							<li>
								<span class="glyphicon glyphicon-briefcase" aria-hidden="true"></span> <?php echo $scholarship; ?>
							</li>
						<?php endif; ?>
						<li>
							<span class="glyphicon glyphicon-calendar" aria-hidden="true"></span> Fecha de publicación: <?php echo get_the_date(); ?>
						</li>
					</ul>
					<input type="hidden" name="job-data" value="<?php echo get_the_title(); ?>">
				</div>
				<hr>
				<div class="job-single-aim">
					<h3>Objetivo:</h3>
					<div>
						<?php echo get_post_meta(get_the_ID(), 'dgk-job-aim', true); ?>
					</div>
				</div>
				<div class="job-single-content">
					<h3>Descripción:</h3>
					<div>
						<?php the_content(); ?>
					</div>
				</div>
				<div class="send-CV">
					<button type="button" class="btn btn-primary btn-lg" data-toggle="modal" data-target="#CVModal" data-hover>
						<span class="button__bg"></span>
						<span>APLICAR A LA VACANTE</span>
					</button>
				</div>
			</article>
			<?php get_template_part('partials/cv-modal'); ?>
		<?php endwhile; endif; ?>
	</section>
	<!-- /site-content -->
</div>
<!-- container -->
<?php get_footer(); ?>