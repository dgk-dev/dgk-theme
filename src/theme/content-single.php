<article class="single">
	<h1 class="single-title"><?php the_title(); ?></h1>
	<hr>
	<div class="post-info">
		<div class="date">
			<span class="glyphicon glyphicon-calendar" aria-hidden="true"></span> <?php echo get_the_date(); ?>
		</div>
		<?php
		$categories = get_the_category();
		$separator  = ', ';
		$output     = '';

		if ( $categories ) : ?>
			<div class="tags">
				<span class="glyphicon glyphicon-tags" aria-hidden="true"></span> 
				<?php foreach ( $categories as $category )
					{ $output .= '<a href="' . get_category_link( $category->term_id ) . '" data-hover>' . $category->cat_name . '</a>' . $separator; }
					echo trim( $output, $separator ); 
				?>
			</div>
		<?php endif; ?>
	</div>
	<div class="post-inner-content">
		<?php the_content(); ?>
	</div>
	<?php
	// If comments are open or we have at least one comment, load up the comment template.
	if ( comments_open() || get_comments_number() ) :
		comments_template();
	endif;
	?>
</article>
