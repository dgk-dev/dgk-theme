<?php get_header(); ?>
<!-- container -->
<main class="container" role="main">
	<!-- site-content -->
	<section class="section site-content">
		<article class="page">
			<h1 class="page-title"><?php _e( '404', 'wordpressify' ); ?></h1>
			<br>
			<div class="page-content">
				<p><?php _e( 'Parece que la página no se encuentra en el servidor', 'wordpressify' ); ?></p>
			</div>
		</article>
</main>
	<!-- /site-content -->
</div>
<!-- /container -->
<?php get_footer(); ?>
