<?php get_header(); ?>
<!-- container -->
<div class="container-fluid">	
	<!-- site-content -->
	<section class="site-content">
		<div class="row">
			<div class="col-xs-12">
				<h2 class="page-title">
					Resultados de búsqueda «<?php echo $_GET['s']; ?>» :
				</h2>
				<hr>
			</div>
		</div>
		<!-- blog container -->
		<section class="blog__bottom">
			<?php
				global $wp_query;
				$total_posts = $wp_query->found_posts;
				$posts_per_page = 9;
				$paged = isset($_GET['page']) ? $_GET['page'] : 1;
			?>
			<div class="row">
				<div class="data-item-container"
					data-posttype="post"
					data-content-template=""
					data-maxpages="<?php echo ceil($total_posts/$posts_per_page); ?>"
					data-paged="<?php echo $paged; ?>"
					data-perpage="<?php echo $posts_per_page; ?>"
					data-taxonomy=""
					data-termslug=""
					data-search = "<?php echo $_GET['s']; ?>"
					data-postindex="<?php echo $_POST['post_index'] ?>"
				></div>
			</div>
			<div class="row">
				<div class="col-xs-12">
					<div class="ajax-notifications">
						<div class="dgk-spinner">
							<div class="rect rect1"></div>
							<div class="rect rect2"></div>
							<div class="rect rect3"></div>
							<div class="rect rect4"></div>
							<div class="rect rect5"></div>
							<span class="loading">CARGANDO</span>
						</div>
						<div class="end-reached">
							<strong>-NO HAY MÁS RESULTADOS-</strong>
						</div>
					</div>
					<div class="load-more-container">
						<button type="button" class="btn btn-primary load-more">
							<span class="button__bg"></span>
							<span>CARGAR MÁS</span>
						</button>
					</div>
				</div>
			</div>
		</section>
	</section>
	<!-- /site-content -->
</div>
<!-- /container -->
<?php get_footer(); ?>
