<?php get_header(); ?>
<?php get_template_part( 'partials/cv-modal' ); ?>
<!-- container -->
<div class="container-fluid">
    <!-- site-content -->
    <section class="site-content">
        <article class="page">
            <section class="jobs__top">
                <div class="row">
                    <h1 class="jobs__title">BOLSA DE TRABAJO</h1>
                </div>

                <div class="squares">
                    <svg xmlns="http://www.w3.org/2000/svg" width="497" height="150" viewBox="0 0 497 150">
                        <g fill="#FFF" fill-rule="evenodd">
                            <path d="M35 102L57 102 57 80 35 80zM0 30L15 30 15 15 0 15zM93 30L124 30 124 0 93 0zM224.75 51.25h21.5v-21.5h-21.5v21.5zM214 62h43V19h-43v43zM438 59L462 59 462 35 438 35zM478 103L497 103 497 84 478 84zM409 130L438 130 438 102 409 102zM368 150L381 150 381 138 368 138zM378 58L412 58 412 24 378 24zM313 102L328 102 328 87 313 87z" />
                        </g>
                    </svg>
                </div>
            </section>
            <?php if (have_posts()) : ?>
                <section class="jobs__bottom">
                    <!-- main-column -->
                    <div class="row">
                        <div class="col-xs-12 col-sm-4 col-md-3">
                            <div class="job-filter">
                                <?php
                                    global $wp_query;
                                    $filter_terms = $terms = get_terms( array(
                                        'taxonomy' => 'workstation',
                                        'hide_empty' => false,
                                    ) );
                                    $total_posts = $wp_query->found_posts;
                                    $posts_per_page = $wp_query->get('posts_per_page');
                                    $paged = isset($_GET['page']) ? $_GET['page'] : 1;
                                ?>
                               
                                <?php if(!empty($filter_terms)): ?>
                                    <div class="filter-controls">
                                        <button class="navbar-toggle" type="button" data-toggle="collapse" data-target=".filter-collapse">
                                            <div class="show-filters">Mostrar filtros</div>
                                            <span class="icon-bar"></span>
                                            <span class="icon-bar"></span>
                                            <span class="icon-bar"></span>
                                        </button>
                                    </div>
                                    <nav class="collapse filter-collapse">
                                        <h3 class="filter-title">Vacantes</h3>
                                        <ul class="filter-nav">
                                            <li class="filter-item active">
                                                <a class="filter-link" href="#"
                                                    data-termslug=""
                                                    data-taxonomy=""
                                                    data-count="<?php echo $total_posts ?>"
                                                    data-maxpages="<?php echo ceil($total_posts/$posts_per_page); ?>"
                                                    data-hover>
                                                        Todas (<?php echo $total_posts ?>)
                                                </a>
                                            </li>
                                            <?php foreach($filter_terms as $term): ?>
                                                <li class="filter-item">
                                                    <a class="filter-link" href="#"
                                                    data-termslug="<?php echo $term->slug; ?>"
                                                    data-taxonomy="<?php echo $term->taxonomy; ?>"
                                                    data-count="<?php echo $term->count ?>"
                                                    data-maxpages="<?php echo ceil($term->count/$posts_per_page); ?>"
                                                    data-hover>
                                                        <?php echo $term->name.' ('.$term->count.')' ?>
                                                    </a>
                                                </li>
                                            <?php endforeach; ?>
                                        </ul>
                                    </nav>
                                <?php endif; ?>
                            </div>
                            <div class="send-CV">
                                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#CVModal" data-hover>
                                    <span class="button__bg"></span>
                                    <span>ENVÍANOS TU CV</span>
                                </button>
                            </div>
                        </div>
                        <div class="col-sm-8 col-md-9">
                            <h3 class="data-title">Empleos disponibles</h3>
                            <div class="data-item-container"
                                 data-posttype="dgk-job"
                                 data-content-template="dgk-job-listing"
                                 data-maxpages="<?php echo ceil($total_posts/$posts_per_page); ?>"
                                 data-paged="<?php echo $paged; ?>"
                                 data-perpage="<?php echo $posts_per_page; ?>"
                                 data-taxonomy=""
                                 data-termslug=""
                                 data-search = ""
                                 data-lastindex=""
                            ></div>
                            <div class="ajax-notifications">
                                <div class="dgk-spinner">
                                    <div class="rect rect1"></div>
                                    <div class="rect rect2"></div>
                                    <div class="rect rect3"></div>
                                    <div class="rect rect4"></div>
                                    <div class="rect rect5"></div>
                                    <span class="loading">CARGANDO</span>
                                </div>
                                <div class="end-reached">
                                    <strong>-NO HAY MÁS VACANTES-</strong>
                                </div>
                            </div>
                            <div class="load-more-container">
                                <button type="button" class="btn btn-primary load-more">
                                    <span class="button__bg"></span>
                                    <span>CARGAR MÁS</span>
                                </button>
                            </div>
                        </div>
                    </div>
                    <!-- /main-column -->
                </section>
            <?php else : ?>
            <div class="row">
                <div class="col-xs-12 text-center no-data">
                    <h3>Actualmente no tenemos vacantes</h3>
                    <p>Puedes <a href="#" data-toggle="modal" data-target="#CVModal" data-hover>enviarnos tu CV</a> y nosotros te contactaremos.</p>
                </div>
            </div>
            <?php endif; ?>
        </article>
    </section>
    <!-- /site-content -->
</div>
<!-- /container -->
<?php get_footer(); ?>