<?php
show_admin_bar( false );

// INCLUDES
//resources: CSS JS FONTS
require_once( trailingslashit( get_template_directory() ). '/includes/resources.php' );

//cleanup wordpress head
if(!is_admin()){
    require_once( trailingslashit( get_template_directory() ). '/includes/cleanup.php' );
}

//custom post type para slider
require_once( trailingslashit( get_template_directory() ). '/includes/custom-post-type-slider.php' );

//custom post type para bolsa de trabajo
require_once( trailingslashit( get_template_directory() ). '/includes/custom-post-type-job.php' );

//custom post type para proyectos
require_once( trailingslashit( get_template_directory() ). '/includes/custom-post-type-project.php' );

//filters
require_once( trailingslashit( get_template_directory() ). '/includes/filters.php' );

//funciones de login
require_once( trailingslashit( get_template_directory() ). '/includes/login.php' );

//funciones de user dashboard
require_once( trailingslashit( get_template_directory() ). '/includes/user-dashboard.php' );

//Crear páginas necesarias al activar el theme
add_action("after_switch_theme", "dgk_create_pages");
add_theme_support( 'post-thumbnails' ); 
function dgk_create_pages(){
    if (isset($_GET['activated']) && is_admin()){
        $pages = array(
            array(
                'title' => 'Home',
                'slug' => 'home'
            ),
        );

        foreach($pages as $page){
            $page_check = get_page_by_path($page['slug']);
            
            if(!isset($page_check->ID)){
                $page_title = $page['title'];
                $page_slug = $page['slug'];
                $page_content = '';
                $page_template = '';
                
                $new_page = array(
                    'post_type' => 'page',
                    'post_title' => $page_title,
                    'post_name' => $page_slug,
                    'post_content' => $page_content,
                    'post_status' => 'publish',
                    'post_author' => 1,
                );

                $new_page_id = wp_insert_post($new_page);
                
                if(!empty($page_template)){
                    update_post_meta($new_page_id, '_wp_page_template', $page_template);
                }
            }
        }

        $homepage = get_page_by_path('home');

        if (isset($homepage->ID)){
            update_option( 'page_on_front', $homepage->ID );
            update_option( 'show_on_front', 'page' );
        }
    }
}

add_action('wp_dashboard_setup', 'dgk_dashboard_widgets');
function dgk_dashboard_widgets() {
    global $wp_meta_boxes;
    wp_add_dashboard_widget('custom_help_widget', 'Bienvenido', 'custom_dashboard_message');
}
 
function custom_dashboard_message() {
    echo '<div style="padding:20px 0; text-align:center;"><img src="'.get_template_directory_uri().'/img/logo-wp.png"></div><p><strong>Este es el administrador del sitio DGK</strong></p><p>Desde aquí podrás gestionar los slides del banner principal así como consultar y exportar la información de los usuarios que llenaron el formulario de contacto<p><p>En la sección <strong>Perfil</strong> podrás editar tus datos principales y tu contraseña.';
}

add_action( 'login_enqueue_scripts', 'dgk_login_logo' );
function dgk_login_logo() { ?>
    <style type="text/css">
        body.login{
            background: #ffffff;
        }
        #login h1 a, .login h1 a {
            background-image: url('<?php echo get_template_directory_uri(); ?>/img/logo-wp.png');
            height: 110px;
            width: 200px;
            background-size: 200px 110px;
            background-repeat: no-repeat;
        	padding-bottom: 30px;
        }
    </style>
<?php }

add_filter( 'login_headerurl', 'dgk_loginlogo_url');
function dgk_loginlogo_url($url) {
    return home_url();
}

function is_search_has_results() {
    if ( is_search()) {
        global $wp_query;
        return ( 0 != $wp_query->found_posts );
    }
}

function is_child($pid){
    global $post;

    if (is_int($pid)) {
        if (is_page($pid)) return true;
        $anc = get_post_ancestors($post->ID);
        foreach ($anc as $ancestor) {
            if (is_page() && $ancestor == $pid) return true;
        }
    } elseif (is_string($pid)) {
        $page = get_page_by_path($pid);
        if ($post->ID == $page->ID) return true;

        $anc = get_post_ancestors($post->ID);
        foreach ($anc as $ancestor) {
            if (is_page() && $ancestor == $page->ID) return true;
        }
    }
    return false;
}

// Comments
function dgk_comments($comment, $args, $depth){
	$GLOBALS['comment'] = $comment; ?>
	<li <?php comment_class(); ?> id="comment-<?php comment_ID() ?>">
		<div class="comment-wrap">
			<div class="row">
				<div class="col-xs-12">
					<div class="comment-img">
						<?php echo get_avatar($comment, $args['avatar_size'], null, null, array('class' => array('img-responsive', 'img-circle'))); ?>
					</div>
					<h4 class="comment-author"><?php echo get_comment_author_link(); ?></h4>
					<span class="comment-date">
						<span class="glyphicon glyphicon-calendar" aria-hidden="true"></span>
						<?php printf(__('%1$s, %2$s', 'dgk-theme'), get_comment_date(),  get_comment_time()) ?>
					</span>
				</div>
			</div>
			<div class="row">
				<div class="col-xs-12">
					<div class="comment-body">
						<?php if ($comment->comment_approved == '0'): ?>
							<em><span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span> <?php _e('Comentario esperando aprobación', 'dgk-theme'); ?></em><br/>
						<?php endif; ?>
						
						<?php comment_text(); ?>
					
						<span class="comment-reply">
							<?php comment_reply_link(array_merge($args, array('reply_text' => __('Responder', 'dgk-theme'), 'depth' => $depth, 'max_depth' => $args['max_depth'])), $comment->comment_ID); ?>
						</span>
					</div>
				</div>
			</div>
		</div>
	</li>
	<?php
}

// Comment replies
add_filter('comment_reply_link', 'dgk_replace_reply_link_class');
function dgk_replace_reply_link_class($class){
    $class = str_replace("class='comment-reply-link", "class='comment-reply-link btn btn-primary", $class);
    return $class;
}

// Enqueue comment-reply script
add_action('wp_enqueue_scripts', 'dgk_public_scripts');
function dgk_public_scripts(){
	if (!is_admin()) {
		if (is_singular() && get_option('thread_comments')) {
			wp_enqueue_script('comment-reply');
		}
	}
}


//remove archive pages
add_action( 'template_redirect', 'dgk_remove_archives', 1 );
function dgk_remove_archives(){
    if ( is_author() || is_day() || is_month() || is_year() ) {
        global $wp_query;
        $wp_query->set_404();
        status_header( 404 );
        nocache_headers();
    }
}