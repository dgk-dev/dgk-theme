<?php get_header(); ?>
<!-- container -->
<main class="container" role="main">
	<!-- site-content -->
	<section class="section site-content page">
		<div class="row">
			<div class="col-xs-12">
				<?php
				if ( have_posts() ) :
					while ( have_posts() ) :
						the_post();
						get_template_part( 'content', 'page' );
					endwhile;
				else :
					get_template_part( 'content', 'none' );
				endif;
				?>
			</div>
		</div>
	</section>
	<!-- /site-content -->
</main>
<!-- /container -->
<?php get_footer(); ?>
