<a href="<?php echo get_the_permalink() ?>" class="wow animated fadeInUp" data-hover>
    <div class="row job-item">
        <div class="col-xs-12 col-sm-12 col-md-9">
            <div class="job-item-header">
                <span class="job-item-title"><?php echo get_the_title() ?></span> - <span class="job-location"><?php echo get_post_meta(get_the_ID(), 'dgk-job-location', true); ?></span>
            </div>
            <div class="job-item-aim">
                <?php echo get_post_meta(get_the_ID(), 'dgk-job-aim', true); ?>
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-3">
            <div class="job-item-date">
                <span class="glyphicon glyphicon-calendar" aria-hidden="true"></span> <?php echo get_the_date(); ?>
            </div>
        </div>
    </div>
    <input type="hidden" class="post-index" value="<?php echo $post->post_index; ?>">
</a>