<div class="col-sm-12 col-md-4">
	<article class="blog__item wow animated fadeInUp <?php echo $post->post_index%2 == 0 ? 'blue' : 'white' ?>" data-aos="fade-up">
		<a href="<?php the_permalink(); ?>" data-hover>
			<figure class="thumbnail__img" <?php echo has_post_thumbnail() ? 'style="background-image: url('.get_the_post_thumbnail_url().')"' : ''; ?>></figure>
		</a>
		<div class="caption">
			<h3 class="caption__title"><a href="<?php the_permalink(); ?>" data-hover><?php the_title(); ?></a></h3>
			<p class="caption__description">
				<?php echo get_the_excerpt(); ?>
			</p>
			<div class="row caption__extras">
				<div class="col-sm-5">
					<span class="glyphicon glyphicon-calendar" aria-hidden="true"></span> <?php echo get_the_date(); ?>
				</div>
				<div class="col-sm-7">
				<?php
					$categories = get_the_category();
					$separator  = ', ';
					$output     = '';
					?>
					<?php if($categories): ?>
						<span class="glyphicon glyphicon-tags" aria-hidden="true"></span>&nbsp;
						<?php foreach ( $categories as $category ){
							$output .= '<a href="' . get_category_link( $category->term_id ) . '" data-hover>' . $category->cat_name . '</a>' . $separator; }
								echo trim( $output, $separator ); 
							?>
					<?php endif; ?>
				</div>
			</div>
		</div>
	</article>
	<input type="hidden" class="post-index" value="<?php echo $post->post_index; ?>">
</div>
<?php if(isset($post->post_index) && $post->post_index % 3 == 0): ?>
	<div class="clearfix"></div>
<?php endif; ?>
