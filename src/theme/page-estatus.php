<?php get_header(); ?>
<!-- container -->
<main class="container" role="main">
    <!-- site-content -->
    <section class="site-content">
        <article class="page">
            <?php if (is_user_logged_in()) : ?>
                <?php $project = get_dgk_project(); ?>
                <?php if ($project->have_posts()) : ?>
                    <?php while ($project->have_posts()) : $project->the_post(); ?>
                        <?php
                        set_query_var('current_project', get_the_ID());
                        set_query_var('current_page', 'Estatus General');
                        get_template_part('partials/user-dashboard/project-header');
                        ?>
                        <div class="row project-description">
                            <div class="col-xs-12">
                                <div class="entry-content">
                                    <?php echo do_shortcode(wpautop(get_the_content(), true)); ?>
                                </div>
                            </div>
                        </div>
                        <div class="row project-mvps">
                            <div class="col-xs-12">
                                <?php
                                $mvps = get_post_meta(get_the_ID(), 'dgk-project-mvp', true);
                                ?>
                                <?php if (!empty($mvps)) : ?>
                                    <div class="text-center">
                                        <h3>% Avance</h3>
                                    </div>
                                    <div class="tabccordion mvps">
                                        <ul class="nav nav-tabs">
                                            <?php $mvp_count = 0;
                                            foreach ($mvps as $mvp) : $mvp_count++ ?>
                                                <li class="<?php echo $mvp_count == 1 ? 'active' : '' ?> col-sm-4 ">
                                                    <div class="progress-circle">
                                                        <div class="barOverflow">
                                                            <div class="bar"></div>
                                                        </div>
                                                        <span><?php echo $mvp['percent'] ?></span>%
                                                    </div>
                                                    <a href="#mvp<?php echo $mvp_count ?>" data-toggle="tab" data-hover>
                                                        MVP <?php echo $mvp_count ?>
                                                    </a>
                                                </li>
                                            <?php endforeach; ?>
                                        </ul>
                                        <div class="tab-content">
                                            <?php $mvp_count = 0;
                                            foreach ($mvps as $mvp) : $mvp_count++ ?>
                                                <div class="tab-pane <?php echo $mvp_count == 1 ? 'active' : '' ?>" id="mvp<?php echo $mvp_count ?>">
                                                    <div class="panel panel-default">
                                                        <div class="panel-heading">
                                                            <h4 class="panel-title">
                                                                <a data-toggle="collapse" data-parent=".tab-pane" href="#collapse-mvp<?php echo $mvp_count ?>" data-hover>
                                                                    MVP <?php echo $mvp_count ?>
                                                                    <div class="progress">
                                                                        <div class="progress-bar" role="progressbar" aria-valuenow="<?php echo $mvp['percent'] ?>" aria-valuemin="0" aria-valuemax="100">
                                                                            <span><?php echo $mvp['percent'] ?></span>%
                                                                        </div>
                                                                    </div>
                                                                </a>
                                                            </h4>
                                                        </div>
                                                        <div id="collapse-mvp<?php echo $mvp_count ?>" class="panel-collapse collapse <?php echo $mvp_count == 1 ? 'in' : '' ?>">
                                                            <div class="panel-body">
                                                                <h3><?php echo $mvp['title']; ?></h3>
                                                                <div class="entry-content">
                                                                    <?php echo do_shortcode(wpautop($mvp['description'], true)); ?>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            <?php endforeach; ?>
                                        </div>
                                    </div>
                                <?php endif; ?>
                            </div>
                        </div>
                    <?php endwhile;
                    $project->reset_postdata(); ?>
                <?php else : ?>
                    <?php get_template_part('content', 'none'); ?>
                <?php endif; ?>
            <?php else : ?>
                <?php get_template_part('partials/user-dashboard/content', 'no-loggedin'); ?>
            <?php endif; ?>
        </article>
    </section>
    <!-- /site-content -->
</main>
<!-- /container -->
<?php get_footer(); ?>