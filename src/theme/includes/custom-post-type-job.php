<?php
// Custom post type
add_action( 'init', 'dgk_custom_post_type_job', 0 );
function dgk_custom_post_type_job() {
	$labels = array(
		'name'                => __( 'Vacantes' ),
		'singular_name'       => __( 'Vacante'),
		'menu_name'           => __( 'Vacantes'),
		'parent_item_colon'   => __( 'Vacante padre'),
		'all_items'           => __( 'Todas las Vacantes'),
		'view_item'           => __( 'Ver Vacante'),
		'add_new_item'        => __( 'Agregar Nueva Vacante'),
		'add_new'             => __( 'Agregar Vacante'),
		'edit_item'           => __( 'Editar Vacante'),
		'update_item'         => __( 'Actualizar Vacante'),
		'search_items'        => __( 'Buscar Vacante'),
		'not_found'           => __( 'Vacante no encontrada'),
		'not_found_in_trash'  => __( 'Vacante no encontrada en papelera')
	);
	$args = array(
		'label'               => __( 'dgk-job'),
		'description'         => __( 'Bolsa de trabajo dgk'),
		'labels'              => $labels,
		'supports'            => array( 'title', 'editor', 'thumbnail'),
		'public'              => true,
		'hierarchical'        => false,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'show_in_nav_menus'   => true,
		'show_in_admin_bar'   => true,
		'has_archive'         => true,
		'can_export'          => true,
		'exclude_from_search' => true,
	    'yarpp_support'       => true,
		'publicly_queryable'  => true,
        'capability_type'     => 'post',
        'rewrite'             => array( 'slug' => 'bolsa-de-trabajo' ),
        'menu_icon'           => 'dashicons-megaphone'
    );
	register_post_type( 'dgk-job', $args );
}


// Taxonomies
add_action( 'init', 'dgk_custom_post_type_job_workstation_taxonomy', 0 );
function dgk_custom_post_type_job_workstation_taxonomy() {
 
  $labels = array(
    'name' => _x( 'Puestos', 'taxonomy general name' ),
    'singular_name' => _x( 'Puesto', 'taxonomy singular name' ),
    'search_items' =>  __( 'Buscar puestos' ),
    'all_items' => __( 'Todos los puestos' ),
    'parent_item' => __( 'Puesto padre' ),
    'parent_item_colon' => __( 'Puesto padre:' ),
    'edit_item' => __( 'Editar puesto' ), 
    'update_item' => __( 'Actualizar puesto' ),
    'add_new_item' => __( 'Agregar Nuevo puesto' ),
    'new_item_name' => __( 'Nuevo nombre de puesto' ),
    'menu_name' => __( 'Puestos' ),
  ); 	
 
  register_taxonomy('workstation',array('dgk-job'), array(
    'hierarchical' => true,
    'labels' => $labels,
    'show_ui' => true,
    'show_admin_column' => true,
    'query_var' => true,
    'rewrite' => array( 'slug' => 'workstation' ),
  ));
}

add_action( 'init', 'dgk_custom_post_type_job_area_taxonomy', 0 );
function dgk_custom_post_type_job_area_taxonomy() {
 
  $labels = array(
    'name' => _x( 'Areas', 'taxonomy general name' ),
    'singular_name' => _x( 'Area', 'taxonomy singular name' ),
    'search_items' =>  __( 'Buscar Areas' ),
    'all_items' => __( 'Todas los Areas' ),
    'parent_item' => __( 'Area padre' ),
    'parent_item_colon' => __( 'Area padre:' ),
    'edit_item' => __( 'Editar Area' ), 
    'update_item' => __( 'Actualizar Area' ),
    'add_new_item' => __( 'Agregar Nueva Area' ),
    'new_item_name' => __( 'Nuevo nombre de Area' ),
    'menu_name' => __( 'Areas' ),
  ); 	
 
  register_taxonomy('area',array('dgk-job'), array(
    'hierarchical' => true,
    'labels' => $labels,
    'show_ui' => true,
    'show_admin_column' => true,
    'query_var' => true,
    'rewrite' => array( 'slug' => 'area' ),
  ));
}

// Metabox data
add_action( 'add_meta_boxes', 'dgk_custom_post_type_job_data_meta_box' );
function dgk_custom_post_type_job_data_meta_box() {
    add_meta_box(
        'dgk-job-data-meta-box',
        __( 'Datos de Vacante', 'dgk-theme' ),
        'dgk_custom_post_type_job_data_meta_box_callback',
        'dgk-job',
        'advanced',
        'high'
    );
}


function dgk_custom_post_type_job_data_meta_box_callback( $post ){
    wp_nonce_field( basename( __FILE__ ), 'dgk-job-data-nonce' );
    $stored_meta = get_post_meta( $post->ID );
    ?>
    <table>
        <tbody class="form-table">
            <tr>
                <th style="font-weight:normal">
                    <label for="dgk-job-location"><?php _e( 'Ubicación', 'dgk-theme' )?></label>
                </th>
                <td>
                    <input name="dgk-job-location" type="text" id="dgk-job-location" value="<?php if ( isset ( $stored_meta['dgk-job-location'] ) ) echo $stored_meta['dgk-job-location'][0]; ?>" />
                </td>
            </tr>
            <tr>
                <th style="font-weight:normal">
                    <label for="dgk-job-scholarship"><?php _e( 'Escolaridad', 'dgk-theme' )?></label>
                </th>
                <td>
                    <input name="dgk-job-scholarship" type="text" id="dgk-job-scholarship" value="<?php if ( isset ( $stored_meta['dgk-job-scholarship'] ) ) echo $stored_meta['dgk-job-scholarship'][0]; ?>" />
                </td>
            </tr>
        </tbody>
    </table>
    <?php
}

// Metabox aim
add_action( 'add_meta_boxes', 'dgk_custom_post_type_job_aim_meta_box' );
function dgk_custom_post_type_job_aim_meta_box() {
    add_meta_box(
        'dgk-job-aim-meta-box',
        __( 'Objetivo', 'dgk-theme' ),
        'dgk_custom_post_type_job_aim_meta_box_callback',
        'dgk-job',
        'advanced',
        'high'
    );
}

function dgk_custom_post_type_job_aim_meta_box_callback($post) {          
    $content = get_post_meta($post->ID, 'dgk-job-aim', true);
    wp_editor ( 
        $content , 
        'dgk-job-aim', 
        array ( "media_buttons" => true ) 
    );
}

//Save all data
add_action( 'save_post', 'dgk_custom_post_type_job_save' );
function dgk_custom_post_type_job_save( $post_id ) {
    // verify nonce
    if (!wp_verify_nonce($_POST['dgk-job-data-nonce'], basename(__FILE__)))
        return $post_id;
        
    // check autosave
    if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE)
        return $post_id;
        
    // check permissions
    if ('page' == $_POST['post_type']) {
        if (!current_user_can('edit_page', $post_id))
            return $post_id;
        } elseif (!current_user_can('edit_post', $post_id)) {
            return $post_id;
    }
 
    // Save
    $oldlocation = get_post_meta($post_id, 'dgk-job-location', true);
    $newlocation = $_POST['dgk-job-location']; 
    if ($newlocation != $oldlocation) {
        update_post_meta($post_id, 'dgk-job-location', $newlocation);
    }

    $oldscholarship = get_post_meta($post_id, 'dgk-job-scholarship', true);
    $newscholarship = $_POST['dgk-job-scholarship']; 
    if ($newscholarship != $oldscholarship) {
        update_post_meta($post_id, 'dgk-job-scholarship', $newscholarship);
    }

    $oldaim = get_post_meta($post_id, 'dgk-job-aim', true);
    $newaim = $_POST['dgk-job-aim']; 
    if ($newaim != $oldaim) {
        update_post_meta($post_id, 'dgk-job-aim', $newaim);
    }
}