<?php
// Custom post type
add_action( 'init', 'dgk_custom_post_type_project', 0 );
function dgk_custom_post_type_project() {
	$labels = array(
		'name'                => __( 'Proyectos' ),
		'singular_name'       => __( 'Proyecto'),
		'menu_name'           => __( 'Proyectos'),
		'parent_item_colon'   => __( 'Proyecto padre'),
		'all_items'           => __( 'Todos los Proyectos'),
		'view_item'           => __( 'Ver Proyecto'),
		'add_new_item'        => __( 'Agregar Nuevo Proyecto'),
		'add_new'             => __( 'Agregar Proyecto'),
		'edit_item'           => __( 'Editar Proyecto'),
		'update_item'         => __( 'Actualizar Proyecto'),
		'search_items'        => __( 'Buscar Proyecto'),
		'not_found'           => __( 'Proyecto no encontrado'),
		'not_found_in_trash'  => __( 'Proyecto no encontrado en papelera')
	);
	$args = array(
		'label'               => __( 'dgk-project'),
		'description'         => __( 'Proyectos dgk'),
		'labels'              => $labels,
		'supports'            => array( 'title', 'editor'),
		'public'              => false,
		'hierarchical'        => false,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'show_in_nav_menus'   => true,
		'show_in_admin_bar'   => true,
		'has_archive'         => false,
		'can_export'          => true,
		'exclude_from_search' => true,
		'publicly_queryable'  => false,
        'capability_type'     => 'post',
        'rewrite'             => array( 'slug' => 'proyectos' ),
        'menu_icon'           => 'dashicons-analytics'
    );
	register_post_type( 'dgk-project', $args );
}

add_action( 'admin_enqueue_scripts', 'dgk_custom_post_type_project_scripts' );
function dgk_custom_post_type_project_scripts(){
    $screen = get_current_screen();
    if($screen->post_type == 'dgk-project'){
        wp_enqueue_style('select2', 'https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/css/select2.min.css' );
        wp_enqueue_script('select2', 'https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/js/select2.min.js', array('jquery') );
        
        wp_enqueue_style('jquery-ui', 'https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css');
        wp_enqueue_script('jquery-ui', 'https://code.jquery.com/ui/1.12.1/jquery-ui.js', array('jquery') );
        
        wp_enqueue_style('dgk-project', get_stylesheet_directory_uri() . '/admin/css/projects.css' );
        wp_enqueue_script('dgk-project', get_stylesheet_directory_uri() . '/admin/js/projects.js', array( 'jquery', 'jquery-ui', 'select2' ) ); 
    }
}


// Metabox data
add_action( 'add_meta_boxes', 'dgk_custom_post_type_project_metaboxes' );
function dgk_custom_post_type_project_metaboxes() {
    add_meta_box(
        'dgk-project-owner-meta-box',
        __( 'Datos de Usuario', 'dgk-theme' ),
        'dgk_custom_post_type_project_owner_meta_box_callback',
        'dgk-project',
        'advanced',
        'high'
    );

    add_meta_box(
        'dgk-project-mvp-meta-box',
        __( 'MVPs', 'dgk-theme' ),
        'dgk_custom_post_type_project_mvp_meta_box_callback',
        'dgk-project',
        'advanced',
        'high'
    );

    add_meta_box(
        'dgk-project-scope-meta-box',
        __( 'Alcance', 'dgk-theme' ),
        'dgk_custom_post_type_project_scope_meta_box_callback',
        'dgk-project',
        'advanced',
        'high'
    );

    add_meta_box(
        'dgk-project-invoices-meta-box',
        __( 'Facturas', 'dgk-theme' ),
        'dgk_custom_post_type_project_invoices_meta_box_callback',
        'dgk-project',
        'advanced',
        'high'
    );
}

function dgk_custom_post_type_project_owner_meta_box_callback( $post ){
    wp_nonce_field( basename( __FILE__ ), 'dgk-project-nonce' );
    $owner = get_post_meta( $post->ID, 'dgk-project-owner',true );
	$main = get_post_meta( $post->ID, 'dgk-project-main',true );
    ?>
    <table class="form-table">
        <tbody>
            <tr>
                <th>
                    <label for="dgk-project-owner"><?php _e( 'Usuario', 'dgk-theme' )?></label>
                </th>
                <td>
                    <select style="width: 100%" name="dgk-project-owner" id="dgk-project-owner">
                        <?php if($owner): $user = get_userdata($owner); ?>
                            <option value="<?php echo $user->ID ?>" selected="selected"><?php echo $user->display_name; ?></option>
                        <?php endif; ?>
                    </select>
                </td>
            </tr>
            <tr>
                <th>
                    <?php _e('Proyecto principal', 'dgk-theme') ?>
                </th>
                <td>
                    <label for="dgk-project-main">
                        <input type="checkbox" name="dgk-project-main" id="dgk-project-main" <?php echo $main ? 'checked="checked"' : '' ?> value="1">
                        Principal
                    </label>
                </td>
            </tr>
        </tbody>
    </table>
    <?php
}

add_action( 'wp_ajax_dgk_project_get_owner', 'dgk_project_get_owner' );
function dgk_project_get_owner(){
    $response = array();

    $args = array(
        'fields' => array( 'ID', 'display_name' ),
        'search' => '*'.esc_attr($_GET['q']).'*',
        'search_columns' => array('display_name')
    );
 
    $users = get_users( $args );
    foreach($users as $user) {
        $response[] = array('id' => $user->ID, 'name' => $user->display_name);
    }
	wp_send_json($response, 200);
}

function dgk_custom_post_type_project_mvp_meta_box_callback( $post ){
    $mvps = get_post_meta( $post->ID, 'dgk-project-mvp', true );
    $mvpCount = 0;
    ?>

    <div id="mvp-append">
        <?php
        if($mvps):
            foreach($mvps as $mvp){
                $mvpCount++;
                dgk_project_render_mvp($mvpCount, $mvp);
            }
        endif;
        ?>
    </div>
    <input type="hidden" id="mvp-index" value="<?php echo $mvpCount ?>">
    <a href="#" id="add-mvp" class="add-mvp button-secondary">Agregar MVP</a>
    <?php
}

add_action( 'wp_ajax_dgk_project_add_mvp', 'dgk_project_add_mvp' );
function dgk_project_add_mvp() {        
    $id = $_GET['id'];
    echo dgk_project_render_mvp($id);
    wp_die();
}

function dgk_project_render_mvp($id, $data=false){
    ?>
    <div class="dgk-project-mvp-container" id="dgk-project-mvp-<?php echo $id ?>" data-index="<?php echo $id ?>">
        <h3 class="handle"><strong>MVP: <span class="mvp-id"><?php echo $id ?></span></strong> - <span class="mvp-title"><?php echo $data ? $data['title'] : '' ?></span></h3>
        <table class="form-table">
            <tbody>
                <tr>
                    <th>
                        <label for="dgk-project-mvp[<?php echo $id ?>][title]"><?php _e( 'Título de MVP', 'dgk-theme' )?></label>
                    </th>
                    <td>
                        <input class="mvp-title-input" data-mvp-id="#dgk-project-mvp-<?php echo $id ?>" name="dgk-project-mvp[<?php echo $id ?>][title]" type="text" id="dgk-project-mvp[<?php echo $id ?>][title]" value="<?php echo $data ? $data['title'] : '' ?>" />  
                    </td>
                </tr>
                <tr>
                    <th>
                        <label for="dgk-project-mvp[<?php echo $id ?>][percent]"><?php _e( 'Porcentaje', 'dgk-theme' )?></label>
                    </th>
                    <td>
                        <input name="dgk-project-mvp[<?php echo $id ?>][percent]" type="text" id="dgk-project-mvp[<?php echo $id ?>][percent]" value="<?php echo $data ? $data['percent'] : '' ?>" />  
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <?php
                        wp_editor ( 
                            $data ? $data['description'] : '' , 
                            'dgk-project-mvp-editor-'.$id, 
                            array ( "media_buttons" => true, 'tinymce' => true, 'textarea_name' => 'dgk-project-mvp['.$id.'][description]', 'textarea_rows' => 20 ) 
                        );
                        ?>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <a href="#" class="remove-mvp" data-mvp-id="#dgk-project-mvp-<?php echo $id ?>">Eliminar MVP</a>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
    <?php
}

function dgk_custom_post_type_project_scope_meta_box_callback( $post ){
	$scope = get_post_meta( $post->ID, 'dgk-project-scope',true );
    wp_editor (
        $scope, 
        'dgk-project-scope', 
        array ( "media_buttons" => true, 'tinymce' => true ) 
    );
}

function dgk_custom_post_type_project_invoices_meta_box_callback( $post ){
	$invoices = get_post_meta( $post->ID, 'dgk-project-invoices',true );
    ?>
    <table class="form-table">
        <tbody>
            <tr>
                <td>
                    <input type='hidden' name='dgk-project-invoices-hidden' id='dgk-project-invoices-hidden' value=<?php echo $invoices; ?>>
                    <div id="invoice-append">
                        <?php if($invoices): ?>
                            <?php foreach(explode(",", $invoices) as $invoice): $invoice_data = get_post( $invoice, ARRAY_A ); ?>
                                <div class="dgk-project-invoice-container" id="dgk-project-invoice-<?php echo $invoice_data['ID'] ?>" data-index="<?php echo $invoice_data['ID'] ?>">
                                    <a class="invoice-link" href="<?php echo $invoice_data['guid']; ?>" target="_BLANK"><?php echo $invoice_data['post_title']; ?></a><br>
                                    <a class="remove-invoice" href="#" data-invoice-id="#dgk-project-invoice-<?php echo $invoice_data['ID']; ?>">Eliminar factura</a>
                                </div>
                            <?php endforeach; ?>
                        <?php endif; ?>
                    </div>
                    <a href="#" id="add-invoice" class="add-invoice button-secondary">Agregar Factura</a>
                </td>
            </tr>
        </tbody>
    </table>
    <?php
}

//Save all data
add_action( 'save_post', 'dgk_custom_post_type_project_save' );
function dgk_custom_post_type_project_save( $post_id ) {
    // verify nonce
    if (!wp_verify_nonce($_POST['dgk-project-nonce'], basename(__FILE__)))
        return $post_id;
        
    // check autosave
    if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE)
        return $post_id;
        
    // check permissions
    if ('page' == $_POST['post_type']) {
        if (!current_user_can('edit_page', $post_id))
            return $post_id;
        } elseif (!current_user_can('edit_post', $post_id)) {
            return $post_id;
    }
 
    // Save
    $oldowner = get_post_meta($post_id, 'dgk-project-owner', true);
    $newowner = $_POST['dgk-project-owner']; 
    if ($newowner != $oldowner) {
        update_post_meta($post_id, 'dgk-project-owner', $newowner);
    }

    $oldmain = get_post_meta($post_id, 'dgk-project-main', true);
    $newmain = $_POST['dgk-project-main']; 
    if ($newmain != $oldmain) {
        update_post_meta($post_id, 'dgk-project-main', $newmain);
    }

    $oldmvp = get_post_meta($post_id, 'dgk-project-mvp', true);
    $newmvp = $_POST['dgk-project-mvp']; 
    if ($newmvp != $oldmvp) {
        update_post_meta($post_id, 'dgk-project-mvp', $newmvp);
    }

    $oldscope = get_post_meta($post_id, 'dgk-project-scope', true);
    $newscope = $_POST['dgk-project-scope']; 
    if ($newscope != $oldscope) {
        update_post_meta($post_id, 'dgk-project-scope', $newscope);
    }

    $oldinvoices = get_post_meta($post_id, 'dgk-project-invoices', true);
    $newinvoices = $_POST['dgk-project-invoices-hidden']; 
    if ($newinvoices != $oldinvoices) {
        update_post_meta($post_id, 'dgk-project-invoices', $newinvoices);
    }
}