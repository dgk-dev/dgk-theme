<?php

add_action('wp_enqueue_scripts', 'dgk_resources', 1);
function dgk_resources(){
	wp_enqueue_style('style', get_stylesheet_uri());
	wp_enqueue_style('google-fonts', 'https://fonts.googleapis.com/css?family=Poppins:300,400,500&display=swap', false);
	wp_enqueue_script('footer_js', get_template_directory_uri() . '/js/footer-bundle.js', null, 1.0, true);
	wp_localize_script('footer_js', 'footerGlobalObject', array(
		'ajax_url' => admin_url('admin-ajax.php')
	));
}

add_action('wp_footer', 'dgk_data');
function dgk_data(){
?>
	<script type="application/ld+json">
		{
			"@context": "http://schema.org",
			"@type": "Organization",
			"name": "dgk",
			"url": "<?php echo home_url(); ?>",
			"address": "Panamá 20, Las Américas, 58270 Morelia, Mich.",
			"image": "<?php echo get_template_directory_uri(); ?>/img/lg-dgk.png",
			"description": "Somos tu agencia digital y ahí está nuestro eje, en poder organizar, desarrollar y representarte  frente a  un mercado constantemente cambiante.",
			"sameAs": [
				"https://www.linkedin.com/company/dgk-mexico",
				"https://www.facebook.com/dgkmx",
				"https://www.instagram.com/dgk_mexico",
				"https://twitter.com/dgk_mx"
			]
		}
	</script>
<?php
}

add_action('wp_footer', 'dgk_analytics');
function dgk_analytics(){
	?>
	<!-- Global site tag (gtag.js) - Google Analytics -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-141650685-1"></script>
      <script>
        window.dataLayer = window.dataLayer || [];

        function gtag() {
          dataLayer.push(arguments);
        }
        gtag('js', new Date());

        gtag('config', 'UA-141650685-1');
      </script>
	<?php
}

//add_action('wp_footer', 'dgk_hubspot');
function dgk_hubspot(){
	?>
	<!-- Start of HubSpot Embed Code -->
	<script type="text/javascript" id="hs-script-loader" async defer src="//js.hs-scripts.com/7224220.js"></script>
	<!-- End of HubSpot Embed Code -->
	<?php
}

add_action('wp_head', 'dgk_fb_pixel');
function dgk_fb_pixel(){
	?>
	<!-- Facebook Pixel Code -->
	<script>
	!function(f,b,e,v,n,t,s)
	{if(f.fbq)return;n=f.fbq=function(){n.callMethod?
	n.callMethod.apply(n,arguments):n.queue.push(arguments)};
	if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
	n.queue=[];t=b.createElement(e);t.async=!0;
	t.src=v;s=b.getElementsByTagName(e)[0];
	s.parentNode.insertBefore(t,s)}(window, document,'script',
	'https://connect.facebook.net/en_US/fbevents.js');
	fbq('init', '205064227448795');
	fbq('track', 'PageView');
	</script>
	<noscript><img height="1" width="1" style="display:none"
	src="https://www.facebook.com/tr?id=205064227448795&ev=PageView&noscript=1"
	/></noscript>
	<!-- End Facebook Pixel Code -->
	<?php
}