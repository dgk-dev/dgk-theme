<?php

function get_dgk_project(){
    $project_id = isset($_GET['project_id']) ? $_GET['project_id'] : 0;
    global $current_user;
    wp_get_current_user();
    $args = array(
        'post_type' => 'dgk-project',
        'posts_per_page'  => 1,
        'post_status'     => 'publish',
        'meta_query' => array(
            array(
                'key'         => 'dgk-project-owner',
                'value'       => $current_user->ID,
            )
        )
    );

    if ($project_id) {
        $args['p'] = $project_id;
    } else {
        $args['meta_key'] = 'dgk-project-main';
        $args['orderby'] = array(
            'meta_value' => 'DESC',
            'ID' => 'DESC'
        );
    }

    return new WP_Query($args);
}

// Redirect if user enter to wp-admin
add_action( 'admin_init', 'dgk_restrict_admin_with_redirect', 1 );
function dgk_restrict_admin_with_redirect() {

    if ( ! current_user_can( 'manage_options' ) && ( ! wp_doing_ajax() ) ) {
        wp_safe_redirect( get_permalink(get_page_by_path('user-dashboard/estatus')) );
        exit;
    }
}

