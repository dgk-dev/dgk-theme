<?php
function dgk_ajax_login(){
    // First check the nonce, if it fails the function will break
    check_ajax_referer( 'dgk-ajax-login-nonce', 'nonce' );

    $response = array();

    $info = array(
        'user_login' => $_POST['user_login'],
        'user_password' => $_POST['user_password'],
        'remember' => $_POST['rememberme']
    );
 
    $user_signon = wp_signon( $info, false );
    if ( is_wp_error($user_signon) ){
        $response['loggedin'] = false;
        $response['message'] = $user_signon->get_error_message('anr_error') ? 'reCAPTCHA inválido' : 'Usuario o contraseña incorrectos';
    }else{
        $response['loggedin'] = true;
        $response['message'] = '<span class="glyphicon glyphicon-ok" aria-hidden="true"></span>';
        $response['redirect'] = $user_signon->has_cap('manage_options') ? get_admin_url() : get_permalink(get_page_by_path('user-dashboard/estatus'));
    }

    wp_send_json($response, 200);
}
add_action('wp_ajax_nopriv_dgk_ajax_login', 'dgk_ajax_login');