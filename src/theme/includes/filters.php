<?php

function dgk_load_more(){
    
    $args = array(  
        'post_type' => $_POST['data']['postType'],
        'post_status' => 'publish',
        'posts_per_page' => $_POST['data']['perPage'], 
        'paged' => $_POST['data']['paged']
    );

    if($_POST['data']['search']){
        unset($args['post_type']);
        $args['s'] = $_POST['data']['search'];
    }

    if(!empty($_POST['data']['taxonomy']) && !empty($_POST['data']['termslug'])){
        $args['tax_query'] =  array(
            array(
                'taxonomy'         => $_POST['data']['taxonomy'],
                'terms'            => $_POST['data']['termslug'],
                'field'            => 'slug'
            )
        );
    }

    $loop = new WP_Query( $args ); 

    $output = '';

    if($loop -> have_posts()):
        $count= isset($_POST['data']['lastIndex']) ? (int) $_POST['data']['lastIndex'] : 0;
        while($loop -> have_posts()): $loop->the_post();
            global $post;
            $count++;
            $post->post_index = $count;
            $output .= get_template_part( 'content', $_POST['data']['contentTemplate']);
        endwhile;
    endif;
    wp_reset_postdata();
    return $output;
}

add_action('wp_ajax_dgk_load_more', 'dgk_load_more');
add_action('wp_ajax_nopriv_dgk_load_more', 'dgk_load_more');