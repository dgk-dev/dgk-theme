<div class="row">
	<div class="col-sm-8 col-lg-5 search-form-container">
		<form class="search-form" role="search" method="get" id="searchform" action="<?php echo home_url( '/' ); ?>">
			<div class="input-group">
				<span class="input-group-btn">
					<button class="btn btn_search" type="submit" data-hover>
						<svg id="icon-search_icon" viewBox="0 0 32 32" class="icon icon-search">
							<title>search_icon</title>
							<path d="M6.135 14.501c0-4.622 3.746-8.365 8.366-8.365s8.364 3.746 8.364 8.365c0 4.62-3.745 8.365-8.365 8.365s-8.364-3.744-8.365-8.364v-0zM28.444 26.965l-6.345-6.344c1.347-1.654 2.163-3.786 2.163-6.11 0-0.004 0-0.008 0-0.011v0.001c0-5.39-4.37-9.76-9.76-9.76s-9.76 4.37-9.76 9.76c0 5.39 4.37 9.76 9.76 9.76v0c2.318 0 4.444-0.811 6.119-2.162l6.344 6.345 1.479-1.479z"></path>
						</svg>
					</button>
				</span>
				<input type="text" class="form-control" name="s" id="s" placeholder="" value="">
			</div>
		</form>
	</div>
</div>